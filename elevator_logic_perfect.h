
#ifndef ELEVATOR_LOGIC_PERFECT
#define ELEVATOR_LOGIC_PERFECT

#include "common_defs.h"
#include "elevator_logic.h"
#include "elevator.h"

class ElevatorLogicPerfect : public ElevatorLogic {
 public:
  ElevatorLogicPerfect(Elevator **es, int ecount);
  void call(int floor, ButtonDirection dir);
  void selectFloor(Elevator *e, int floor);
  void notifyFloor(Elevator *e, int floorBefore, int floorAfter);
};

#endif

