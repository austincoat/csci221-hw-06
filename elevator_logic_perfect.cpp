#include <iostream>
#include <iomanip>
#include "common_defs.h"
#include "elevator.h"
#include "elevator_logic_perfect.h"
#include <list>
using namespace std;

list<int> intList;
list<int> callList;
ElevatorLogicPerfect::ElevatorLogicPerfect(Elevator **es, int ecount)
	:ElevatorLogic(es,ecount)
{
 
}


void ElevatorLogicPerfect::call(int floor, ButtonDirection dir)
{
 callList.push_front(floor);
if(es[0]->getMotorDirection()==M_HALTED)
{
if(callList.back()> es[0]->getCurrentFloor())
 {
 	es[0]->setMotorDirection(M_UP);
 }
 else if( callList.back() < es[0]->getCurrentFloor())         
 {                                                  
	es[0]->setMotorDirection(M_DOWN);
 }
 if( callList.back() == es[0]->getCurrentFloor())
 {
        es[0]->setMotorDirection(M_HALTED);
	selectFloor(es[0],callList.back());
        callList.pop_back();
 }
}
}

void ElevatorLogicPerfect::selectFloor(Elevator *e, int floor)
{
 intList.push_front(floor);
	for(int i=0;i<intList.size();i++)
{
  if(es[0]->getCurrentFloor()< intList.back())
  {
	es[0]->setMotorDirection(M_UP);
  }
  if(es[0]->getCurrentFloor()>intList.back()) 
  {                                                        
	 es[0]->setMotorDirection(M_DOWN);
  } 
  if(es[0]->getCurrentFloor()== intList.back())
  {
	es[0]->setMotorDirection(M_HALTED);
  	intList.pop_back();
  }
}
}


void ElevatorLogicPerfect::notifyFloor(Elevator *e, int floorBefore, int floorAfter)
{

 
}

