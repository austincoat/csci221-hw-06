#ifndef ELEVATOR_H
#define ELEVATOR_H

#include "common_defs.h"

#include <cassert>
#include <list>
#include <iostream>
#include <string>

#include "elevator_group_simulator.h"

class Elevator {
 private:
  std::string name;
  int currentFloor;
  MotorDirection motorDirection;
  std::list<int> log;
  void step();
  /**
   * Reset currentFloor to 1, set motorDirection to M_HALTED, and clear the log.
   */
  void reset();

  friend void ElevatorGroupSimulator::step();
  friend void ElevatorGroupSimulator::reset();

 public:
  Elevator(std::string name);
  std::string getName();
  std::list<int> &getLog();
  int getCurrentFloor();
  MotorDirection getMotorDirection();
  void setMotorDirection(MotorDirection md);
  bool isHalted();
};

#endif
