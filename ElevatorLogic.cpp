#include <iostream>
#include <elevator.h>

using namespace std;

ElevatorLogic(Elevator **es, int ecount)
{
    this->es = es;
    this->ecount = ecount;
}

void call(int floor, ButtonDirection dir)
{
 if(floor > this->getCurrentFloor())
 {
 	this->setMotorDirection(up);
 }
 else if(floor < this->getCurrentFloor())                
 {                                                  
	this->setMotorDirection(down);
 }
 else if(floor == this->getCurrentFloor())
 { 
	this->halt();
 }
}

void selectFloor(Elevator *e, int floor)
{
 if(this->getCurrentFloor()<floor)
 {
	this-> setMotorDirection(up);
 }
 else if(this->getCurrentFloor()>floor)                 
 {                                                 
        this-> setMotorDirection(down);
 } 
 else if(this->getCurrentFloor()==floor)
 {
	this->halt();
 }
}

void notifyFloor(Elevator *e, int floorBefore, int floorAfter)
{
 
}
